# Bluemix Node.js App Sample using GitLab CI - CD (Deploying) to IBM Cloud!

The application repo uses GitLab CI/CD to Continuously Deploy your Applications to IBM Cloud.

## Node.js Starter Overview

The Node.js Starter demonstrates a simple, reusable Node.js web application based on the Express framework.

## Run the app locally

1. [Install Node.js](https://nodejs.org/en/download/)
2. Clone this repository in GitLab
3. Run `npm install` to install the app's dependencies
4. Run `npm test` to lint all the source files
5. Run `npm start` to start the app
6. Access the running app in a browser at https://gitlab-ibm-host.mybluemix.net/
